<?php
     
    /*----------------------------------------------------------------------------*
     *  DISABLE WORDPRESS THEME UPDATES
     *----------------------------------------------------------------------------*/

    remove_action( 'load-update-core.php', 'wp_update_themes' );

    add_filter( 'pre_site_transient_update_themes', '__return_zero' );