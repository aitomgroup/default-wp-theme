/*!
 * Gulp + Bedrock + WordPress
 * 
 * @author Jan Semilsky
 * @version 1.0.2
 * 
 * Install:
 * install node.js
 * npm install
 * install "LiveReload" plugin to Google Chrome, https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?hl=en
 * 
 * Use:
 * gulp			(build production mode frontend)
 * gulp watch	(watch changes on frontend and use livereload)
 */

// Load plugins
var del = require('del'),
    gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
	less = require('gulp-less'),
    livereload = require('gulp-livereload'),
	cleanCss = require('gulp-clean-css'),
	uglify = require('gulp-uglify'),
	using = require('gulp-using'),
    themeBase = 'app/themes/default/',
    assetsBase = themeBase + 'assets/';

var fs = require('fs');

var jsFiles = JSON
		.parse(fs.readFileSync('html/assets/js/scripts.json'))
		.map(function(item) {
			return item = 'html/assets/js/' + item;
		});

// ********************************* CSS ************************************

// processes all styles
function compileStyle(args) {
    gulp.src('html/assets/style/style.less')
        .pipe(less({relativeUrls: true}))
        .pipe(autoprefixer({
            browsers: ['last 10 versions', 'ie 8-11'],
            cascade: false
        }))
        .pipe(cleanCss({compatibility: 'ie9', format: args.format ? args.format : false}))
        .pipe(gulp.dest('html/assets/css'))
        .pipe(gulp.dest(assetsBase + 'css'));
}

gulp.task('styles', function() {
	compileStyle({format: falsee});
});

gulp.task('stylesDev', function() {
	compileStyle({format: 'beautify'});
});

// ********************************* Scripts ********************************

function compileScripts(args) {
    gulp.src(jsFiles)
        .pipe(concat('aitom.main.js'))
        .pipe(uglify({output: { beautify: args.beautify ? args.beautify : false }}))
        .pipe(gulp.dest('html/assets/js/min'))
        .pipe(gulp.dest(assetsBase + 'js/min'));
}

gulp.task('scripts', function() {
	compileScripts({beautify: false});
});

gulp.task('scriptsDev', function() {
	compileScripts({beautify: true});
});

// ********************************** Images ********************************

gulp.task('images', function() {
	if ( fs.existsSync( themeBase + 'style.css' ) ) {
        gulp.src('html/assets/images/**/*')
			.pipe(gulp.dest(assetsBase + 'images/'));
    }
});

// ********************************* utils ***********************************

// Clean
gulp.task('clean', function() {
	return del(['html/assets/css/style.css', assetsBase + 'css/style.css', 'html/assets/js/min', assetsBase + 'js/min', assetsBase + 'images/*']);
});

// ********************************* TASKS ***********************************

// Default task - build production mode
gulp.task('default', ['clean'], function() {
	gulp.start('styles', 'scripts', 'images');
});

// Watch task - build development mode
gulp.task('watch', function() {

	// build dev styles
	gulp.start('stylesDev');

	// build dev scripts
	gulp.start('scriptsDev');

	// Watch .less files
	gulp.watch('html/assets/style/**/*.less', { usePolling: true }, function() {
		gulp.start('stylesDev');
	});

	// Watch scripts files
	gulp.watch(['html/assets/js/**/*', '!html/assets/js/min/*'], { usePolling: true }, function() {
		gulp.start('scriptsDev');
	});
    
    // Watch images
	gulp.watch('html/assets/images/**/*', { usePolling: true }, function() {
		gulp.start('images');
	});

	// Watch any html files, reload on change
    gulp.watch(['html/**/*.html']).on('change', function() {
		livereload.reload();
	});
    
    // Create LiveReload server
	livereload.listen();
});

module.exports = gulp;