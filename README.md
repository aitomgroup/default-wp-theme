# Výchozí WordPress šablona

Výchozí šablona AITOM pro Bedrock sloužící pro vývoj šablony na míru. Kodér připraví html šablony a programátor pak z nich vytvoří WordPress šablonu. Styly, javascripty a obrázky (assets) se kopírují z html do WordPress šablony automaticky pomocí gulp.

## Instalace

Nainstalovat šablonu do Bedrock projektu můžeš ručně zkopírováním souborů 'gulpfile.js', 'package.json' a složky 'html' do rootu projektu a následně složku 'default' do adresáře 'app/themes'.

Nebo můžeš nainstalovat pomocí Composeru - přidáním do composer.json:


      "repositories": [
        {
          "type": "vcs",
          "url": "https://bitbucket.org/aitomgroup/default-wp-theme.git"
        }
      ],
      "require-dev": {
        "aitomgroup/default-theme": "dev-master"
      }

A následně v command line zadat tyto příkazy:

      sudo cp -R vendor/aitomgroup/default-theme/gulpfile.js .
      
      sudo cp -R vendor/aitomgroup/default-theme/package.json .
      
      sudo cp -R vendor/aitomgroup/default-theme/html .
      
      sudo cp -R vendor/aitomgroup/default-theme/default ./app/themes/

Úpravu composer.json do repozitáže necomituj. Naopak složka 'html' a složka šablony 'default' by součástí repozitáře být měla.

Kodér pak připravuje html šablony v podadresáři html (např. www.bedrock.dev/html), v rootu má spuštěný gulp.